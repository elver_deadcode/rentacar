@extends('layout.app')

@section('title', "Rent Car")

@section('content')

<!-- CONTENT AREA -->
    <div class="content-area">

        <!-- PAGE -->
        <section class="page-section no-padding slider">
            <div class="container full-width">

                <div class="main-slider">
                    <div class="" id="main-slider">

                        <!-- Slide 2 -->
                        <div class="item slide2 ver2">
                            <div class="caption">
                                <div class="container">
                                    <div class="div-table">
                                        <div class="div-cell">
                                            <div class="caption-content">
                                                <!-- Search form -->
                                                <div class="form-search light">
                                                    <form action="{{route('buscar-auto')}}" method="post">
                                                    {{ csrf_field() }}
                                                        <div class="form-title">
                                                            <i class="fa fa-globe"></i>
                                                            <h2>Search for Cheap Rental Cars Wherever Your Are</h2>
                                                        </div>

                                                        <div class="row row-inputs">
                                                            <div class="container-fluid">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group has-icon has-label selectpicker-wrapper">
                                                                        <label for="formSearchUpLocation2">Sucursal de Salida</label>
                                                                        <select  required class="selectpicker" data-live-search="true" data-width="100%"
                                                                                data-toggle="tooltip" title="Seleccione una sucursal" name='sucursalSalida'>
                                                                                <option value="" selected>Seleccione una sucursal</option>
                                                                            @foreach ($datos as $sucursal)
                                                                                <option value="{{$sucursal['Codigo']}}">{{$sucursal['Nombre']}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <span class="form-control-icon"><i class="fa fa-location-arrow"></i></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="form-group has-icon has-label selectpicker-wrapper">
                                                                        <label for="formSearchOffLocation2">Sucursal de Devolución</label>
                                                                        <select required class="selectpicker" data-live-search="true" data-width="100%"
                                                                                data-toggle="tooltip" title="Seleccione una sucursal" name='sucursalDevolucion'>

                                                                                <option value="" selected>Seleccione una sucursal</option>
                                                                            @foreach ($datos as $sucursal)
                                                                                <option value="{{$sucursal['Codigo']}}">{{$sucursal['Nombre']}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <span class="form-control-icon"><i class="fa fa-location-arrow"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row row-inputs">
                                                            <div class="container-fluid">
                                                                <div class="col-sm-7">
                                                                    <label for="formSearchUpDate2">Fecha de Salida</label>
                                                                   <div class="input-group date fechas" id="ReportDate">
                                                                    <div id="txtdatefrom">
                                                                        <input required type="text" class="fechas form-control  fechasalida" name='fechaSalida' id='fechaSalida' data-date-format="DD/MM/YYYY">
                                                                    </div>
                                                                    <span class="input-group-addon text-pointer">
                                                                    <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <div class="form-group has-icon has-label">
                                                                        <label for="formSearchUpHour2">Hora de Salida</label>
                                                                        <input  required type="text" class="form-control datepicker" name='horaSalida' id='horaSalida' data-date-format="HH:mm">
                                                                        <span class="form-control-icon"><i class="fa fa-clock-o"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row row-inputs">
                                                            <div class="container-fluid">

                                                                  <div class="col-sm-7">
                                                                    <label for="formSearchUpDate2">Fecha de Devolucion</label>
                                                                   <div class="input-group date fechas" id="ReportDate">
                                                                    <div id="txtdatefrom">
                                                                        <input type="text" class="fechas form-control fechadevo " name='fechaDevolucion' id='fechaDevolucion' data-date-format="DD/MM/YYYY">
                                                                    </div>
                                                                    <span class="input-group-addon text-pointer">
                                                                    <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    </div>
                                                                    
                                                                </div>
                                                             
                                                                <div class="col-sm-5">
                                                                    <div class="form-group has-icon has-label">
                                                                        <label for="formSearchOffHour2">Hora de Devolución</label>
                                                                        <input type="text" class="form-control datepicker" name='horaDevolucion' id='horaDevolucion' data-date-format="HH:mm">
                                                                        <span class="form-control-icon"><i class="fa fa-clock-o"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row row-submit">
                                                            <div class="container-fluid">
                                                                <div class="inner">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                    <button type="submit" id="submit" class="btn btn-submit btn-theme pull-right">Buscar</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- /Search form -->


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Slide 2 -->

                        <!-- Slide 1 -->
                        {{-- <div class="item slide1 ver1">
                            <div class="caption">
                                <div class="container">
                                    <div class="div-table">
                                        <div class="div-cell">
                                            <div class="caption-content">
                                                <h2 class="caption-title">All Discounts Just For You</h2>
                                                <h3 class="caption-subtitle">Find Best Rental Car</h3>
                                                <!-- Search form -->
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-10 col-md-offset-1">

                                                        <div class="form-search dark">
                                                            <form action="#">
                                                                <div class="form-title">
                                                                    <i class="fa fa-globe"></i>
                                                                    <h2>Search for Cheap Rental Cars Wherever Your Are</h2>
                                                                </div>

                                                                <div class="row row-inputs">
                                                                    <div class="container-fluid">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group has-icon has-label">
                                                                                <label for="formSearchUpLocation">Picking Up Location</label>
                                                                                <input type="text" class="form-control" id="formSearchUpLocation" placeholder="Airport or Anywhere">
                                                                                <span class="form-control-icon"><i class="fa fa-map-marker"></i></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="form-group has-icon has-label">
                                                                                <label for="formSearchUpDate">Picking Up Date</label>
                                                                                <input type="text" class="form-control datepicker" id="formSearchUpDate" placeholder="dd/mm/yyyy">
                                                                                <span class="form-control-icon"><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="form-group has-icon has-label">
                                                                                <label for="formSearchUpHour">Picking Up Hour</label>
                                                                                <input type="text" class="form-control" id="formSearchUpHour" placeholder="20:00 AM">
                                                                                <span class="form-control-icon"><i class="fa fa-clock-o"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row row-inputs">
                                                                    <div class="container-fluid">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group has-icon has-label">
                                                                                <label for="formSearchOffLocation">Dropping Off Location</label>
                                                                                <input type="text" class="form-control" id="formSearchOffLocation" placeholder="Airport or Anywhere">
                                                                                <span class="form-control-icon"><i class="fa fa-map-marker"></i></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="form-group has-icon has-label">
                                                                                <label for="formSearchOffDate">Dropping Off Date</label>
                                                                                <input type="text" class="form-control datepicker" id="formSearchOffDate" placeholder="dd/mm/yyyy">
                                                                                <span class="form-control-icon"><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="form-group has-icon has-label">
                                                                                <label for="formSearchOffHour">Dropping Off Hour</label>
                                                                                <input type="text" class="form-control" id="formSearchOffHour" placeholder="20:00 AM">
                                                                                <span class="form-control-icon"><i class="fa fa-clock-o"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="row row-submit">
                                                                    <div class="container-fluid">
                                                                        <div class="inner">
                                                                            <i class="fa fa-plus-circle"></i> <a href="#">Advanced Search</a>
                                                                            <button type="submit" id="formSearchSubmit" class="btn btn-submit btn-theme pull-right">Find Car</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- /Search form -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <!-- /Slide 1 -->

                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE -->

        <!-- PAGE -->
        <section class="page-section">
            <div class="container">

                <div class="row">
                    <div class="col-md-4 wow flipInY" data-wow-offset="70" data-wow-duration="1s">
                        <div class="thumbnail thumbnail-featured no-border no-padding">
                            <div class="media">
                                <a class="media-link" href="#">
                                    <div class="caption">
                                        <div class="caption-wrapper div-table">
                                            <div class="caption-inner div-cell">
                                                <div class="caption-icon"><i class="fa fa-support"></i></div>
                                                <h4 class="caption-title">Asistencia 24 horas</h4>
                                                <div class="caption-text">Seguro a todo riesgo en todos nuestros behículos.</div>
                                                <div class="buttons">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption hovered">
                                        <div class="caption-wrapper div-table">
                                            <div class="caption-inner div-cell">
                                                <div class="caption-icon"><i class="fa fa-support"></i></div>
                                                <h4 class="caption-title">Asistencia 24 horas</h4>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 wow flipInY" data-wow-offset="70" data-wow-duration="1s" data-wow-delay="200ms">
                        <div class="thumbnail thumbnail-featured no-border no-padding">
                            <div class="media">
                                <a class="media-link" href="#">
                                    <div class="caption">
                                        <div class="caption-wrapper div-table">
                                            <div class="caption-inner div-cell">
                                                <div class="caption-icon"><i class="fa fa-calendar"></i></div>
                                                <h4 class="caption-title">Reservas garantizadas</h4>
                                                <div class="caption-text">Realice su reserva cuando y donde usted quiera. Sin ataduras.</div>
                                                <div class="buttons">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption hovered">
                                        <div class="caption-wrapper div-table">
                                            <div class="caption-inner div-cell">
                                                <div class="caption-icon"><i class="fa fa-calendar"></i></div>
                                                <h4 class="caption-title">Reservas garantizadas</h4>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 wow flipInY" data-wow-offset="70" data-wow-duration="1s" data-wow-delay="400ms">
                        <div class="thumbnail thumbnail-featured no-border no-padding">
                            <div class="media">
                                <a class="media-link" href="#">
                                    <div class="caption">
                                        <div class="caption-wrapper div-table">
                                            <div class="caption-inner div-cell">
                                                <div class="caption-icon"><i class="fa fa-map-marker"></i></div>
                                                <h4 class="caption-title">Su coche cerca de usted</h4>
                                                <div class="caption-text">Disponemos de varios puntos de recogida. Entrega directa.</div>
                                                <div class="buttons">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="caption hovered">
                                        <div class="caption-wrapper div-table">
                                            <div class="caption-inner div-cell">
                                                <div class="caption-icon"><i class="fa fa-map-marker"></i></div>
                                                <h4 class="caption-title">Su coche cerca de usted</h4>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE -->



        <!-- PAGE -->
        <section class="page-section dark">
            <div class="container">

                <h2 class="section-title wow fadeInUp" data-wow-offset="70" data-wow-delay="100ms">

                    <span>Nuestra gama</span>
                </h2>

                <div class="tabs wow fadeInUp" data-wow-offset="70" data-wow-delay="300ms">
                    <ul id="tabs" class="nav">
					<li class=""><a href="#tab-3" data-toggle="tab">Familiares</a></li>
                        <li class=""><a href="#tab-1" data-toggle="tab">Económicos</a></li>
                        <li class="active"><a href="#tab-2" data-toggle="tab">Exclusivos</a></li>
						<li class=""><a href="#tab-4" data-toggle="tab">Carga</a></li>

                    </ul>
                </div>

                <div class="tab-content wow fadeInUp" data-wow-offset="70" data-wow-delay="500ms">

                    <!-- tab 1 -->
                    <div class="tab-pane fade" id="tab-1">

                        <div class="swiper swiper--offers-best">
                            <div class="swiper-container">

                                <div class="swiper-wrapper">
                                    <!-- Slides -->
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/500.jpg">
                                                    <img src="assets/img/preview/cars/ma/500.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">FIAT 500</a></h4>
                                                <div class="caption-text">Desde 29€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2017</td>
                                                        <td><i class="fa fa-dashboard"></i> Gasolina</td>
                                                        <td><i class="fa fa-cog"></i> Auto</td>
                                                        <td><i class="fa fa-road"></i> <25000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/ibiza.jpg">
                                                    <img src="assets/img/preview/cars/ma/ibiza.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">SEAT IBIZA 1.1 TSI</a></h4>
                                                <div class="caption-text">Desde 29€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2016</td>
                                                        <td><i class="fa fa-dashboard"></i> Gasolina</td>
                                                        <td><i class="fa fa-cog"></i> Auto</td>
                                                        <td><i class="fa fa-road"></i> >50000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/fabia.jpg">
                                                    <img src="assets/img/preview/cars/ma/fabia.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">SKODA FABIA 1.1 TSI</a></h4>
                                                <div class="caption-text">Desde 29€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2017</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Manual </td>
                                                        <td><i class="fa fa-road"></i> 25000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/panda.jpg">
                                                    <img src="assets/img/preview/cars/ma/panda.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">FIAT PANDA 1.0</a></h4>
                                                <div class="caption-text">Desde 25€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2016</td>
                                                        <td><i class="fa fa-dashboard"></i> Gasolina</td>
                                                        <td><i class="fa fa-cog"></i> Manual</td>
                                                        <td><i class="fa fa-road"></i> <25000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>
                            <div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>

                        </div>

                    </div>

                    <!-- tab 2 -->
                    <div class="tab-pane fade active in" id="tab-2">

                        <div class="swiper swiper--offers-popular">
                            <div class="swiper-container">

                                <div class="swiper-wrapper">
                                    <!-- Slides -->
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/mercedesslk.jpg">
                                                    <img src="assets/img/preview/cars/ma/mercedesslk.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">MERCEDES SLK 370</a></h4>
                                                <div class="caption-text">Desde 129€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2017</td>
                                                        <td><i class="fa fa-dashboard"></i> Gasolina</td>
                                                        <td><i class="fa fa-cog"></i> Manual</td>
                                                        <td><i class="fa fa-road"></i> 25000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/mercedesb.jpg">
                                                    <img src="assets/img/preview/cars/ma/mercedesb.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">MERCEDES B 270</a></h4>
                                                <div class="caption-text">Desde 67€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2013</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Auto</td>
                                                        <td><i class="fa fa-road"></i> 25000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
									 <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/x1.jpg">
                                                    <img src="assets/img/preview/cars/ma/x1.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">BMW X1 370</a></h4>
                                                <div class="caption-text">Desde 75€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2017</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Auto</td>
                                                        <td><i class="fa fa-road"></i> 25000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/vito.jpg">
                                                    <img src="assets/img/preview/cars/ma/vito.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">MERCEDES VITO 2.0 TDI</a></h4>
                                                <div class="caption-text">Desde 49€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2016</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Auto</td>
                                                        <td><i class="fa fa-road"></i> <75000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <!-- If we need navigation buttons -->
                            <div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>
                            <div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>

                        </div>

                    </div>

                    <!-- tab 3 -->
                    <div class="tab-pane fade" id="tab-3">

                        <div class="swiper swiper--offers-economic">
                            <div class="swiper-container">

                                <div class="swiper-wrapper">
                                    <!-- Slides -->
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/touran.jpg">
                                                    <img src="assets/img/preview/cars/ma/touran.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">VW TOURAN 2.0 TDI </a></h4>
                                                <div class="caption-text">Desde 49€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2015</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Auto</td>
                                                        <td><i class="fa fa-road"></i> <25000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/picasso.jpg">
                                                    <img src="assets/img/preview/cars/ma/picasso.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">CITROEN PICASSO 2.0 TDI</a></h4>
                                                <div class="caption-text">Desde  49€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2017</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Manual</td>
                                                        <td><i class="fa fa-road"></i> <45000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/zafira.jpg">
                                                    <img src="assets/img/preview/cars/ma/zafira.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#">OPEL ZAFIRA 1.7 HDI</a></h4>
                                                <div class="caption-text">Desde 49€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2016</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Auto</td>
                                                        <td><i class="fa fa-road"></i> <65000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="thumbnail no-border no-padding thumbnail-car-card">
                                            <div class="media">
                                                <a class="media-link" data-gal="prettyPhoto" href="assets/img/preview/cars/ma/dacia.jpg">
                                                    <img src="assets/img/preview/cars/ma/dacia.jpg" alt=""/>
                                                    <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="#"> DACIA LODGY 1.5 D</a></h4>
                                                <div class="caption-text">Desde 39€ / día</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme ripple-effect" href="#">Reservar</a>
                                                </div>
                                                <table class="table">
                                                    <tr>
                                                        <td><i class="fa fa-car"></i> 2017</td>
                                                        <td><i class="fa fa-dashboard"></i> Diesel</td>
                                                        <td><i class="fa fa-cog"></i> Manual</td>
                                                        <td><i class="fa fa-road"></i> <45000</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- If we need navigation buttons -->
                            <div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>
                            <div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>

                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE -->




        <!-- PAGE -->
        <section class="page-section image">
            <div class="container">

                <div class="row">
                    <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-offset="200" data-wow-delay="100ms">
                        <div class="thumbnail thumbnail-counto no-border no-padding">
                            <div class="caption">
                                <div class="caption-icon"><i class="fa fa-heart"></i></div>
                                <div class="caption-number">5657</div>
                                <h4 class="caption-title">Clientes satisfechos</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-offset="200" data-wow-delay="200ms">
                        <div class="thumbnail thumbnail-counto no-border no-padding">
                            <div class="caption">
                                <div class="caption-icon"><i class="fa fa-car"></i></div>
                                <div class="caption-number">1.657</div>
                                <h4 class="caption-title">Vehículos</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-offset="200" data-wow-delay="300ms">
                        <div class="thumbnail thumbnail-counto no-border no-padding">
                            <div class="caption">
                                <div class="caption-icon"><i class="fa fa-flag"></i></div>
                                <div class="caption-number">2.255.657</div>
                                <h4 class="caption-title">Total KMS</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 wow fadeInDown" data-wow-offset="200" data-wow-delay="400ms">
                        <div class="thumbnail thumbnail-counto no-border no-padding">
                            <div class="caption">
                                <div class="caption-icon"><i class="fa fa-comments-o"></i></div>
                                <div class="caption-number">4255</div>
                                <h4 class="caption-title">Asistencias</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE -->

        <!-- PAGE -->
        <section class="page-section">
            <div class="container">

                <h2 class="section-title wow fadeInDown" data-wow-offset="200" data-wow-delay="100ms">
                    <small>Preguntas frecuentes</small>
                    <span>FAQS</span>
                </h2>

                <div class="row">
                    <div class="col-md-6 wow fadeInLeft" data-wow-offset="200" data-wow-delay="200ms">
                        <!-- FAQ -->
                        <div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
                            <!-- faq1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading1">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                            <span class="dot"></span> ¿Cómo puedo realizar una reserva?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                    <div class="panel-body">
                                        En nuestra web, sección reserva podrá consultar la disponibilidad de los distintos vehículos que disponemos y seleccionar el que más le satisfazca para formalizar la reserva.
                                    </div>
                                </div>
                            </div>
                            <!-- /faq1 -->
                            <!-- faq2 -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading2">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                            <span class="dot"></span> ¿Dónde puedo reservar un coche?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                    <div class="panel-body">
                                        Tenemos nuestra oficina base en Dénia pero puede recoger y entregar el vehículo en el Aeropuerto de Valencia o de Alicante.
                                    </div>
                                </div>
                            </div>
                            <!-- /faq2 -->
                            <!-- faq3 -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading3">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                            <span class="dot"></span> ¿El vehículo se entrega con combustible?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                    <div class="panel-body">
                                        En el momento de la entrega del vehiculo usted elige si lo prefiere con el deposito lleno o vacío.
                                    </div>
                                </div>
                            </div>
                            <!-- /faq3 -->
                        </div>
                        <!-- /FAQ -->
                    </div>
                    <div class="col-md-6 wow fadeInRight" data-wow-offset="200" data-wow-delay="200ms">
                        <!-- FAQ -->
                        <div class="panel-group accordion" id="accordion2" role="tablist" aria-multiselectable="true">
                            <!-- faq1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading21">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse21" aria-expanded="false" aria-controls="collapse21">
                                            <span class="dot"></span> ¿Si tengo un accidente que ocurre?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse21" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading21">
                                    <div class="panel-body">
                                        Si tiene algún percance le recomendamos que llame a la policia para que le pueda ayudar. Llame posteriormente a cualquiera de nuestras oficinas para que le ayudemos.
                                    </div>
                                </div>
                            </div>
                            <!-- /faq1 -->
                            <!-- faq2 -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading22">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapse22" aria-expanded="true" aria-controls="collapse22">
                                            <span class="dot"></span> ¿Tienen servicio de urgencia?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse22" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading22">
                                    <div class="panel-body">
                                        si usted tiene una urgencia puede llamarnos al teléfono +34 965 784 988 o enviarnos un email a 24horas@renticm.com.
                                    </div>
                                </div>
                            </div>
                            <!-- /faq2 -->
                            <!-- faq3 -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading23">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse23" aria-expanded="false" aria-controls="collapse23">
                                            <span class="dot"></span> ¿Tienen silla para bebé?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse23" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading23">
                                    <div class="panel-body">
                                        Disponemos de silla bebé como extra. Todos nuestros vehículos utilizan la tecnología isoflix.
                                    </div>
                                </div>
                            </div>
                            <!-- /faq3 -->
                        </div>
                        <!-- /FAQ -->
                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE -->

        <!-- PAGE -->
        <section class="page-section find-car dark">
            <div class="container">

                <form action="#" class="form-find-car">
                    <div class="row">

                        <div class="col-md-3 wow fadeInDown" data-wow-offset="200" data-wow-delay="100ms">

                            <h2 class="section-title text-left no-margin">

                                <span>Encuentre su coche</span>
                            </h2>

                        </div>
                        <div class="col-md-3 wow fadeInDown" data-wow-offset="200" data-wow-delay="200ms">
                            <div class="form-group has-icon has-label">
                                <label for="formFindCarLocation">Lugar recogida</label>
                                <input type="text" class="form-control" id="formFindCarLocation" placeholder="Aeropuerto VAL">
                                <span class="form-control-icon"><i class="fa fa-location-arrow"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2 wow fadeInDown" data-wow-offset="200" data-wow-delay="300ms">
                            <div class="form-group has-icon has-label">
                                <label for="formFindCarDate">Fecha recogida</label>
                                <input type="text" class="form-control datepicker" id="formFindCarDate" placeholder="dd/mm/yyyy">
                                <span class="form-control-icon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2 wow fadeInDown" data-wow-offset="200" data-wow-delay="400ms">
                            <div class="form-group has-icon has-label">
                                <label for="formFindCarCategory">Grupo</label>
                                <input type="text" class="form-control" id="formFindCarCategory" placeholder="Seleccione grupo de vehículo">
                                <span class="form-control-icon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2 wow fadeInDown" data-wow-offset="200" data-wow-delay="500ms">
                            <div class="form-group">
                                <button type="submit" id="formFindCarSubmit" class="btn btn-block btn-submit ripple-effect btn-theme">Buscar</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </section>
        <!-- /PAGE -->

        <!-- PAGE -->
        <section class="page-section">
            <div class="container">

                <h2 class="section-title wow fadeInDown" data-wow-offset="200" data-wow-delay="100ms">
                    <small>Blog</small>
                    <span>Últimos posts</span>
                </h2>

                <div class="row">
                    <div class="col-md-6 wow fadeInLeft" data-wow-offset="200" data-wow-delay="200ms">
                        <div class="recent-post alt">
                            <div class="media">
                                <a class="media-link" href="#">
                                    <div class="badge type">Una Navidad diferente</div>
                                    <div class="badge post"><i class="fa fa-video-camera"></i></div>
                                    <img class="media-object" src="assets/img/preview/blog/navidad.jpg" alt="">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="media-left">
                                    <div class="meta-date">
                                        <div class="day">21</div>
                                        <div class="month">Dec</div>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="media-meta">
                                        By Renticm
                                        <span class="divider">|</span><a href="#"><i class="fa fa-comment"></i>13</a>
                                        <span class="divider">|</span><a href="#"><i class="fa fa-heart"></i>346</a>
                                        <span class="divider">|</span><a href="#"><i class="fa fa-share-alt"></i></a>
                                    </div>
                                    <h4 class="media-heading"><a href="#">Los mejores precios para estas navidades.</a></h4>
                                    <div class="media-excerpt">Las escapadas navideñas son ya un clásico. Descubra las mejores ofertas para esas Navidades. Consulte sus opciones. Consiga los mejores precios</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInRight" data-wow-offset="200" data-wow-delay="200ms">
                        <div class="recent-post alt">
                            <div class="media">
                                <a class="media-link" href="#">
                                    <div class="badge type">Vehículos ecológicos</div>
                                    <div class="badge post"><i class="fa fa-image"></i></div>
                                    <img class="media-object" src="assets/img/preview/blog/eco.jpg" alt="">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="media-left">
                                    <div class="meta-date">
                                        <div class="day">21</div>
                                        <div class="month">Dec</div>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <div class="media-meta">
                                        By admin
                                        <span class="divider">|</span><a href="#"><i class="fa fa-comment"></i>13</a>
                                        <span class="divider">|</span><a href="#"><i class="fa fa-heart"></i>346</a>
                                        <span class="divider">|</span><a href="#"><i class="fa fa-share-alt"></i></a>
                                    </div>
                                    <h4 class="media-heading"><a href="#">Sistemas híbridos</a></h4>
                                    <div class="media-excerpt">Estamos comprometidos en colaborar con el medio ambiente y por ello anunciamos que iremos renovando la flota con vehículos 100% ecológicos.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center margin-top wow fadeInDown" data-wow-offset="200" data-wow-delay="100ms">
                    <a href="#" class="btn btn-theme ripple-effect btn-theme-light btn-more-posts">See All Posts</a>
                </div>

            </div>
        </section>
        <!-- /PAGE -->

        <!-- PAGE -->
        <section class="page-section image subscribe">
            <div class="container">

                <h2 class="section-title wow fadeInDown" data-wow-offset="200" data-wow-delay="100ms">
                    <small>Suscríbase a nuestro boletín</small>
                    <span>Suscribirse</span>
                </h2>

                <div class="row wow fadeInDown" data-wow-offset="200" data-wow-delay="200ms">
                    <div class="col-md-8 col-md-offset-2">

                        <p class="text-center">Aquí puede suscribirse a nuestro boletín informativo. Este boletín es la forma más sencilla de mantenerse al día acerca de las últimas noticias y artículos relacionados con Renticm.</p>

                        <!-- Subscribe form -->
                        <form action="#" class="form-subscribe">
                            <div class="form-group">
                                <label for="formSubscribeEmail" class="sr-only">Inserte su E-Mail</label>
                                <input type="text" class="form-control" id="formSubscribeEmail" placeholder="Enter your email here" title="Email is required">
                            </div>
                            <button type="submit" class="btn btn-submit btn-theme ripple-effect btn-theme-dark">Suscribirse</button>
                        </form>
                        <!-- Subscribe form -->

                    </div>
                </div>

            </div>
        </section>
        <!-- /PAGE -->

    </div>

@endsection

@section('footer')

    <script type="text/javascript">

    $(function(){

         $('.fechas').datepicker({
        language: 'es',
        autoclose:true,
        format:'dd/mm/yyyy',
        showOn: "button",
     
    });
        // $('#fechaSalida').datedatetimepicker()datetimepicker()picker();
        $('#horaSalida').datetimepicker();
        // $('#fechaDevolucion').();
        $('#horaDevolucion').datetimepicker();


        $('.icon_fechasalida').click(function(){
            $('.fechasalida').click();
        })
    })

    </script>

@endsection
