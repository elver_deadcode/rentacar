@extends('layout.app')

@section('title', "Reserve show")

@section('content')

<!-- CONTENT AREA -->
    <div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>{{$auto['Descripcion_Grupo']}}</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active">Reservar {{$auto['Descripcion_Grupo']}}</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

        <!-- PAGE WITH SIDEBAR -->
        <section class="page-section with-sidebar sub-page">
            <div class="container">
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-md-9 content" id="content">

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Información General</h3>

                        <form class="form-extras form-delivery" method="POST" action="{{url('reservar-request')}}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="fechasalida" value="{{$reserva->fechaSalida}}">
                            <input type="hidden" name="fechadevolucion" value="{{$reserva->fechaDevolucion}}">
                            <input type="hidden" name="horadevolucion" value="{{$reserva->horaDevolucion}}">
                            <input type="hidden" name="horasalida" value="{{$reserva->horaSalida}}">
                            <input type="hidden" name="sucdevolucion" value="{{$reserva->sucursalDevolucion}}">
                            <input type="hidden" name="sucsalida" value="{{$reserva->sucursalSalida}}">
                            <input type="hidden" name="grupo" value="{{$reserva->id}}">
                            <div class="car-big-card alt">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="">
                                            <div class="item">
                                                <a class="btn btn-zoom" href="{{asset('assets/img/autos')}}/{{ $auto['CodGrupoRentCar'] }}.jpg" data-gal="prettyPhoto"><i class="fa fa-arrows-h"></i></a>
                                                <a href="{{asset('assets/img/autos')}}/{{ $auto['CodGrupoRentCar'] }}.jpg" data-gal="prettyPhoto"><img class="img-responsive" src="{{asset('assets/img/autos')}}/{{ $auto['CodGrupoRentCar'] }}.jpg" alt=""/></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="car-details">
                                            <div class="list">
                                                <ul>
                                                    <li class="title">
                                                        <h2>{{$auto['Descripcion_Grupo']}}</h2>
                                                      {{$auto["Argumento_Comercial"]}}
                                                    </li>
                                                    <li>Fuel Diesel / 1600 cm3 Engine</li>
                                                    <li>Under 25,000 Km</li>
                                                    <li>Transmission Manual</li>
                                                    <li>5 Year service included</li>
                                                    <li>Manufacturing Year 2014</li>
                                                    <li>5 Doors and Panorama View</li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <strong>€ <span class="precio_iva_label"> {{$reserva->precio_iva}}</span></strong> <i class="fa fa-info-circle"></i>
                                                <input type="hidden" class="precio_iva" value=" {{$reserva->precio_iva}}" name="precio_total">
                                                <input type="hidden" class="total_extras" value="0" name="precio_extras">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="page-divider half transparent"/>

                            @if(count($extras)>0)

                            <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Extras</h3>
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table table-extras table-condensed">
                                        <thead>
                                        <tr>
                                            <th class="col-md-4">Extra</th>
                                            <th>Unidades</th>
                                            <th>Precio con IVA</th>
                                            <th>Seleccionar</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        @foreach($extras as $e)
                                            <tr>
                                                <td>{{$e['DescripcionExtra']}}</td>
                                                <td><input  style="width:55px" class="unidades_extra" type="number" name="unidades_extra[]" value="{{$e['UnidadesExtra']}}"></td>
                                                <td class="precio_extra ">{{$e['PecioExtra_con_IVA']}}</td>
                                                <td>
                                                    <input type="checkbox"  name="sw[]" value="{{$e['CodigoExtra']}}"  class="cb_extra">
                                                <input type="hidden" name="cod_extra[]" value="{{$e['CodigoExtra']}}">
                                                <input type="hidden" name="descripcion_extra[]" value="{{$e['DescripcionExtra']}}">
                                                <input type="hidden" name="unidades_extra[]" value="{{$e['UnidadesExtra']}}">
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>

                            </div>
                            @endif


                            <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Información del Cliente</h3>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="nombre" id=""
                                                class="form-control " required type="text" placeholder="Nombres*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="apellido" id="" required title="Este campo es requerido" data-toggle="tooltip"
                                                class="form-control " type="text" placeholder="Apellidos*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="email" id=""
                                                class="form-control " required type="text" placeholder="Correo electrónico*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="telefono" class="form-control " type="phone" placeholder="Teléfono:">
                                    </div>
                                </div>
                            </div>
                            {{--<hr class="page-divider half transparent"/>--}}

                            {{--<h3 class="block-title alt"><i class="fa fa-angle-down"></i>Métodos de Pago</h3>--}}
                            {{--<div class="panel-group payments-options" id="accordion" role="tablist" aria-multiselectable="true">--}}
                                {{--<div class="panel radio panel-default">--}}
                                    {{--<div class="panel-heading" role="tab" id="headingOne">--}}
                                        {{--<h4 class="panel-title">--}}
                                            {{--<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">--}}
                                                {{--<span class="dot"></span> Efectivo--}}
                                            {{--</a>--}}
                                        {{--</h4>--}}
                                    {{--</div>--}}
                                    {{--<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">--}}
                                        {{--<div class="panel-body">--}}
                                            {{--<div class="alert alert-success" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus.</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="panel panel-default">--}}
                                    {{--<div class="panel-heading" role="tab" id="headingThree">--}}
                                        {{--<h4 class="panel-title">--}}
                                            {{--<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapseThree">--}}
                                                {{--<span class="dot"></span> Tarjeta de crédito--}}
                                            {{--</a>--}}
                                    {{--<span class="overflowed pull-right">--}}
                                        {{--<img src="{{asset('assets/img/preview/payments/mastercard-2.jpg')}}" alt=""/>--}}
                                        {{--<img src="{{asset('assets/img/preview/payments/visa-2.jpg')}}" alt=""/>--}}
                                        {{--<img src="{{asset('assets/img/preview/payments/american-express-2.jpg')}}" alt=""/>--}}
                                        {{--<img src="{{asset('assets/img/preview/payments/discovery-2.jpg')}}" alt=""/>--}}
                                        {{--<img src="{{asset('assets/img/preview/payments/eheck-2.jpg')}}" alt=""/>--}}
                                    {{--</span>--}}
                                        {{--</h4>--}}
                                    {{--</div>--}}
                                    {{--<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3"></div>--}}
                                {{--</div>--}}
                                {{--<div class="panel panel-default">--}}
                                    {{--<div class="panel-heading" role="tab" id="heading4">--}}
                                        {{--<h4 class="panel-title">--}}
                                            {{--<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">--}}
                                                {{--<span class="dot"></span> PayPal--}}
                                            {{--</a>--}}
                                            {{--<span class="overflowed pull-right"><img src="{{asset('assets/img/preview/payments/paypal-2.jpg')}}" alt=""/></span>--}}
                                        {{--</h4>--}}
                                    {{--</div>--}}
                                    {{--<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr class="page-divider half transparent"/>--}}

                            <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Inormación Adicional</h3>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input name="num_vuelo" id=""
                                               class="form-control " required type="text" placeholder="Numero de vuelo*">                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="observaciones" id=""
                                                class="form-control " placeholder="Ingresa aquí comentarios adicionales sobre tu reserva" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="overflowed reservation-now">
                                <div class="checkbox pull-left">
                                    <input id="accept" type="checkbox" name="fd-name" title="Please accept" data-toggle="tooltip">
                                    <label for="accept">Acepto todos los Términos y Condiciones</label>
                                </div>
                                <a class="btn btn-theme pull-right btn-cancel btn-theme-dark" href="#">Cancelar</a>
                                <button type="submit" class="btn btn-procesar btn-theme pull-right " >Reservar</button>
                            </div>

                        </form>

                    </div>
                    <!-- /CONTENT -->

                    <!-- SIDEBAR -->
              @include('includes.sidebar')
                    <!-- /SIDEBAR -->

                </div>
            </div>
        </section>
        <!-- /PAGE WITH SIDEBAR -->

        <!-- PAGE -->
        <section class="page-section contact dark">
            <div class="container">

                <!-- Get in touch -->

                <h2 class="section-title">
                    <small>Feel Free to Say Hello!</small>
                    <span>Get in Touch With Us</span>
                </h2>

                <div class="row">
                    <div class="col-md-6">
                        <!-- Contact form -->
                        <form name="contact-form" method="post" action="#" class="contact-form alt" id="contact-form">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="name">Name</label>
                                            <input
                                                    type="text" name="name" id="name" placeholder="Name" value="" size="30"
                                                    data-toggle="tooltip" title="Name is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-user"></i></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="email">Email</label>
                                            <input
                                                    type="text" name="email" id="email" placeholder="Email" value="" size="30"
                                                    data-toggle="tooltip" title="Email is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group af-inner has-icon">
                                <label class="sr-only" for="input-message">Message</label>
                                <textarea
                                        name="message" id="input-message" placeholder="Message" rows="4" cols="50"
                                        data-toggle="tooltip" title="Message is required"
                                        class="form-control placeholder"></textarea>
                                <span class="form-control-icon"><i class="fa fa-bars"></i></span>
                            </div>

                            <div class="outer required">
                                <div class="form-group af-inner">
                                    <input type="submit" name="submit" class="form-button form-button-submit btn btn-block btn-theme" id="submit_btn" value="Send message" />
                                </div>
                            </div>

                        </form>
                        <!-- /Contact form -->
                    </div>
                    <div class="col-md-6">

                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>

                        <ul class="media-list contact-list">
                            <li class="media">
                                <div class="media-left"><i class="fa fa-home"></i></div>
                                <div class="media-body">Adress: 1600 Pennsylvania Ave NW, Washington, D.C.</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa"></i></div>
                                <div class="media-body">DC 20500, ABD</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-phone"></i></div>
                                <div class="media-body">Support Phone: 01865 339665</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-envelope"></i></div>
                                <div class="media-body">E mails: info@example.com</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-clock-o"></i></div>
                                <div class="media-body">Working Hours: 09:30-21:00 except on Sundays</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-map-marker"></i></div>
                                <div class="media-body">View on The Map</div>
                            </li>
                        </ul>

                    </div>
                </div>

                <!-- /Get in touch -->

            </div>
        </section>
        <!-- /PAGE -->

    </div>
    <!-- /CONTENT AREA -->
@section('scripts')
    <script>
        $(document).ready(function(){

            $('.btn-procesar').click(function(_evt){
                _evt.preventDefault();


                if($('#accept').is(':checked')){

                    $('.form-extras').submit();

                }
                else{
                    alert('Es necesario que acepte los terminos y condiciones')
                    return false;
                }

            })

             $('.unidades_extra').change(function(){
                // let total_extras=parseFloat($('.total_extras').val())
                // let monto=parseFloat($(this).closest('tr').find('.precio_extra').html())
                // let cant=parseFloat($(this).closest('tr').find('.unidades_extra').val())

                
                //      monto=monto*cant
                // let piva=parseFloat($('.precio_iva').val())
                // let total=0;
                // if($(this).is(':checked')){

                //      total=piva+monto
                //     total_extras+=monto;
                // }
                // else{
                //      total=piva-monto
                //     total_extras-=monto;
                // }

                // $('.precio_iva').val(total.toFixed(2))
                // $('.precio_iva_label').html(total.toFixed(2))
                // $('.total_extras').val(total_extras.toFixed(2))

            })
            

            $('.cb_extra').click(function(){
                let total_extras=parseFloat($('.total_extras').val())
                let monto=parseFloat($(this).closest('tr').find('.precio_extra').html())
                let cant=parseFloat($(this).closest('tr').find('.unidades_extra').val())

                
                     monto=monto*cant
                let piva=parseFloat($('.precio_iva').val())
                let total=0;
                if($(this).is(':checked')){

                     total=piva+monto
                    total_extras+=monto;
                    $(this).closest('tr').find('.unidades_extra').attr('readonly',true)
                }
                else{
                     total=piva-monto
                    total_extras-=monto;
                                        $(this).closest('tr').find('.unidades_extra').attr('readonly',false)

                }

                $('.precio_iva').val(total.toFixed(2))
                $('.precio_iva_label').html(total.toFixed(2))
                $('.total_extras').val(total_extras.toFixed(2))

            })
        })
    </script>

@stop
@endsection
