@extends('layout.app')

@section('title', "Reserve show")

@section('content')

    <!-- CONTENT AREA -->
    <div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>RESERVA COMPLETADA</h1>
                </div>

            </div>
        </section>
        
        <!-- /BREADCRUMBS -->

        <!-- PAGE WITH SIDEBAR -->
        <section class="page-section with-sidebar sub-page">
            <div class="container">
                <div class="row">
                    <a href="{{url('/')}}" class="btn btn-default"><i class="fa fa-backward"></i> Ir al Inicio</a>
                    <!-- CONTENT -->
                    @if(count($reserva)>0)
                    <div class="col-md-12 content" id="content">


                        <div class="div-print">
                        <h3 class="block-title alt"></i>DETALLES DE LA RESERVA</h3>
                            <ul class="list-unstyled">
                                <li><strong>Numero de la Reserva: </strong> {{$reserva['NumeroReservaRS']}}</li>
                                <li><strong>Descripcion: </strong> {{$reserva['DescricionGrupoRS']}}</li>
                                <li><strong>Cliente: </strong> {{$reserva['NombreClienteRS']}}</li>
                                <li><strong>Email: </strong> {{$reserva['EMailClienteRS']}}</li>
                                <li><strong>Fecha devolucion: </strong> {{$reserva['FechaDevolucionRS']}}</li>
                                <li><strong>Hora devolucion: </strong> {{$reserva['HoraDevolucionRS']}}</li>
                                <li><strong>Sucursal Devolucion: </strong> {{$reserva['SucDevolucionRS']}}</li>
                                <li><strong>Fecha Salida: </strong> {{$reserva['FechaSalidaRS']}}</li>
                                <li><strong>Hora Salida: </strong> {{$reserva['HoraSalidaRS']}}</li>
                                <li><strong>Sucursal Salida: </strong> {{$reserva['SucSalidaRS']}}</li>
                                <li><strong>Total de extras con IVA: </strong> {{$reserva['TotalExtrasConIvaRS']}}</li>
                                <li><strong>Total de tarifa con IVA: </strong> {{$reserva['TotalTarifaConIvaRS']}}</li>
                            </ul>
                              @if(count($extras)>0)
                              <h5 class="block-title alt">Extras</h4>
                               <ul class="list-unstyled">

                                @foreach($extras  as $k=>$e)
                               <li><strong>Nombre: </strong> {{$e['NombreRS']}} <strong>Unidades: </strong>{{$e['UnidadesRS']}}</li>
                               @endforeach
                            </ul>

                              @endif


                            </div>
                            
                            
                            
                            <button class="btn-print  btn btn-primary"><i class=" fa fa-print"></i> IMPRIMIR</button>
                        </div>
                        @endif

                </div>
            </div>
        </section>
        <!-- /PAGE WITH SIDEBAR -->

        <!-- PAGE -->
        <section class="page-section contact dark">
            <div class="container">

                <!-- Get in touch -->

                <h2 class="section-title">
                    <small>Feel Free to Say Hello!</small>
                    <span>Get in Touch With Us</span>
                </h2>

                <div class="row">
                    <div class="col-md-6">
                        <!-- Contact form -->
                        <form name="contact-form" method="post" action="#" class="contact-form alt" id="contact-form">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="name">Name</label>
                                            <input
                                                    type="text" name="name" id="name" placeholder="Name" value="" size="30"
                                                    data-toggle="tooltip" title="Name is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-user"></i></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="email">Email</label>
                                            <input
                                                    type="text" name="email" id="email" placeholder="Email" value="" size="30"
                                                    data-toggle="tooltip" title="Email is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group af-inner has-icon">
                                <label class="sr-only" for="input-message">Message</label>
                                <textarea
                                        name="message" id="input-message" placeholder="Message" rows="4" cols="50"
                                        data-toggle="tooltip" title="Message is required"
                                        class="form-control placeholder"></textarea>
                                <span class="form-control-icon"><i class="fa fa-bars"></i></span>
                            </div>

                            <div class="outer required">
                                <div class="form-group af-inner">
                                    <input type="submit" name="submit" class="form-button form-button-submit btn btn-block btn-theme" id="submit_btn" value="Send message" />
                                </div>
                            </div>

                        </form>
                        <!-- /Contact form -->
                    </div>
                    <div class="col-md-6">

                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>

                        <ul class="media-list contact-list">
                            <li class="media">
                                <div class="media-left"><i class="fa fa-home"></i></div>
                                <div class="media-body">Adress: 1600 Pennsylvania Ave NW, Washington, D.C.</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa"></i></div>
                                <div class="media-body">DC 20500, ABD</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-phone"></i></div>
                                <div class="media-body">Support Phone: 01865 339665</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-envelope"></i></div>
                                <div class="media-body">E mails: info@example.com</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-clock-o"></i></div>
                                <div class="media-body">Working Hours: 09:30-21:00 except on Sundays</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-map-marker"></i></div>
                                <div class="media-body">View on The Map</div>
                            </li>
                        </ul>

                    </div>
                </div>

                <!-- /Get in touch -->

            </div>
        </section>
        <!-- /PAGE -->

    </div>
    <!-- /CONTENT AREA -->
@section('scripts')
    <script>
        $(document).ready(function(){

        
            $('.btn-print').click(function(){
               var options = { mode : "iframe", popClose : close,  retainAttr : ['class','style','id'] };

            
            $('.div-print').printArea(options);


            })
          
        })
    </script>

@stop
@endsection
