@extends('layout.app')

@section('title', "Car List")

@section('content')

<!-- CONTENT AREA -->
    <div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>Reservar auto</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active">Reservar auto</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

        <!-- PAGE WITH SIDEBAR -->
        <section class="page-section with-sidebar sub-page">
            <div class="container">
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-md-9 content car-listing" id="content">
                        <!-- Car Listing -->
                        @if( isset($autos['MensajeError']))
                        <div class="">
                            <h4 class="text-danger">{{$autos['MensajeError']}}</h4>
                            <a href="{{url('/')}}" class="btn btn-default" > <i class=" fa fa-backward"></i> Regresar</a>
                        </div>
                            
                            
                        @endif
                        @if( count($autos)>0)
                        @foreach($autos as $auto)
                            @if(isset($auto['Descripcion_Grupo']) && $auto['Descripcion_Grupo'] != null)
                            <div class="thumbnail no-border no-padding thumbnail-car-card clearfix">
                                <div class="media">
                                    <a class="media-link" data-gal="prettyPhoto" href="{{asset('assets/img/autos')}}/{{ $auto['CodGrupoRentCar'] }}.jpg">
                                        <img src="{{asset('assets/img/autos')}}/{{ $auto['CodGrupoRentCar'] }}.jpg" alt=""/>
                                        <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                    </a>
                                </div>
                                <div class="caption">
                                    <div class="rating">
                                        <span class="star"></span>
                                        <span class="star active"></span>
                                        <span class="star active"></span>
                                        <span class="star active"></span>
                                        <span class="star active"></span>
                                    </div>
                                    <h4 class="caption-title"><a href="#">{{ $auto['Descripcion_Grupo'] }}</a></h4>
                                    <h5 class="caption-title-sub text-primary">Total: {{$auto['Pecio_con_IVA']}} Euros. IVA incluido.</h5>
                                    <div class="caption-text">{{ $auto['Argumento_Comercial'] }}</div>
                                    <table class="table">
                                        <tr>
                                            {{--<td><i class="fa fa-car"></i> 2013</td>--}}
                                            <td>Musica @if($auto['Musica']==1) <i class="fa fa-check text-success"></i> @else <i class="fa fa-close"></i> @endif</td>
                                            <td>Puertas: {{$auto['Puertas']}}</td>
                                            <td>Plazas : {{$auto['Plazas']}}</td>
                                            <td class="buttons">

                                            <form action="{{route('reservar-auto')}}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{$auto['CodGrupoRentCar']}}">
                                                <input type="hidden" name="sucursalSalida" value="{{$reserva->sucursalSalida}}">
                                                <input type="hidden" name="precio_iva" value="{{$auto['Pecio_con_IVA']}}">
                                                <input type="hidden" name="sucursalDevolucion" value="{{$reserva->sucursalDevolucion}}">
                                                <input type="hidden" name="fechaSalida" value="{{$reserva->fechaSalida}}">
                                                <input type="hidden" name="fechaDevolucion" value="{{$reserva->fechaDevolucion}}">
                                                <input type="hidden" name="horaSalida" value="{{$reserva->horaSalida}}">
                                                <input type="hidden" name="horaDevolucion" value="{{$reserva->horaDevolucion}}">
                                                <input type="hidden" name="extras" value="{{$extras}}">
                                                <input type="submit" class="btn btn-theme" value="Reservar">
                                            </form>

                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>


                                @endif
                        @endforeach
                             @else


                        @endif

                        <!-- /Car Listing -->

                        <!-- Pagination
                        <div class="pagination-wrapper">
                            <ul class="pagination">
                                <li class="disabled"><a href="#"><i class="fa fa-angle-double-left"></i> Previous</a></li>
                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">Next <i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </div>
                        Pagination -->

                    </div>
                    <!-- /CONTENT -->

                    <!-- SIDEBAR -->
                  @include('includes.sidebar')
                    <!-- /SIDEBAR -->

                </div>
            </div>
        </section>
        <!-- /PAGE WITH SIDEBAR -->

        <!-- PAGE -->
        <section class="page-section contact dark">
            <div class="container">

                <!-- Get in touch -->

                <h2 class="section-title">
                    <small>Feel Free to Say Hello!</small>
                    <span>Get in Touch With Us</span>
                </h2>

                <div class="row">
                    <div class="col-md-6">
                        <!-- Contact form -->
                        <form name="contact-form" method="post" action="#" class="contact-form alt" id="contact-form">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="name">Name</label>
                                            <input
                                                    type="text" name="name" id="name" placeholder="Name" value="" size="30"
                                                    data-toggle="tooltip" title="Name is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-user"></i></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="email">Email</label>
                                            <input
                                                    type="text" name="email" id="email" placeholder="Email" value="" size="30"
                                                    data-toggle="tooltip" title="Email is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group af-inner has-icon">
                                <label class="sr-only" for="input-message">Message</label>
                                <textarea
                                        name="message" id="input-message" placeholder="Message" rows="4" cols="50"
                                        data-toggle="tooltip" title="Message is required"
                                        class="form-control placeholder"></textarea>
                                <span class="form-control-icon"><i class="fa fa-bars"></i></span>
                            </div>

                            <div class="outer required">
                                <div class="form-group af-inner">
                                    <input type="submit" name="submit" class="form-button form-button-submit btn btn-block btn-theme" id="submit_btn" value="Send message" />
                                </div>
                            </div>

                        </form>
                        <!-- /Contact form -->
                    </div>
                    <div class="col-md-6">

                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>

                        <ul class="media-list contact-list">
                            <li class="media">
                                <div class="media-left"><i class="fa fa-home"></i></div>
                                <div class="media-body">Adress: 1600 Pennsylvania Ave NW, Washington, D.C.</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa"></i></div>
                                <div class="media-body">DC 20500, ABD</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-phone"></i></div>
                                <div class="media-body">Support Phone: 01865 339665</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-envelope"></i></div>
                                <div class="media-body">E mails: info@example.com</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-clock-o"></i></div>
                                <div class="media-body">Working Hours: 09:30-21:00 except on Sundays</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-map-marker"></i></div>
                                <div class="media-body">View on The Map</div>
                            </li>
                        </ul>

                    </div>
                </div>

                <!-- /Get in touch -->

            </div>
        </section>
        <!-- /PAGE -->

    </div>
    <!-- /CONTENT AREA -->

@endsection
