@extends('layout.app')

@section('title', "Show Auto")

@section('content')

<!-- CONTENT AREA -->
    <div class="content-area">

        <!-- BREADCRUMBS -->
        <section class="page-section breadcrumbs text-right">
            <div class="container">
                <div class="page-header">
                    <h1>{{$auto['Descripcion_Grupo']}}</h1>
                </div>
                <ul class="breadcrumb">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active">Booking & Payment</li>
                </ul>
            </div>
        </section>
        <!-- /BREADCRUMBS -->

        <!-- PAGE WITH SIDEBAR -->
        <section class="page-section with-sidebar sub-page">
            <div class="container">
                <div class="row">
                    <!-- CONTENT -->
                    <div class="col-md-9 content" id="content">

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Información General</h3>
                        <div class="car-big-card alt">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="owl-carousel img-carousel">
                                        <div class="item">
                                            <a class="btn btn-zoom" href="{{asset('assets/img/preview/cars/car-600x426x1.jpg')}}" data-gal="prettyPhoto"><i class="fa fa-arrows-h"></i></a>
                                            <a href="{{asset('assets/img/preview/cars/car-600x426x1.jpg')}}" data-gal="prettyPhoto"><img class="img-responsive" src="a{{asset('ssets/img/preview/cars/car-600x426x1.jpg')}}" alt=""/></a>
                                        </div>
                                        <div class="item">
                                            <a class="btn btn-zoom" href="{{asset('assets/img/preview/cars/car-600x426x2.jpg')}}" data-gal="prettyPhoto"><i class="fa fa-arrows-h"></i></a>
                                            <a href="{{asset('assets/img/preview/cars/car-600x426x2.jpg')}}" data-gal="prettyPhoto"><img class="img-responsive" src="{{asset('assets/img/preview/cars/car-600x426x2.jpg')}}" alt=""/></a>
                                        </div>
                                        <div class="item">
                                            <a class="btn btn-zoom" href="{{asset('assets/img/preview/cars/car-600x426x1.jpg')}}" data-gal="prettyPhoto"><i class="fa fa-arrows-h"></i></a>
                                            <a href="{{asset('assets/img/preview/cars/car-600x426x1.jpg')}}" data-gal="prettyPhoto"><img class="img-responsive" src="{{asset('assets/img/preview/cars/car-600x426x1.jpg')}}" alt=""/></a>
                                        </div>
                                        <div class="item">
                                            <a class="btn btn-zoom" href="{{asset('assets/img/preview/cars/car-600x426x2.jpg')}}" data-gal="prettyPhoto"><i class="fa fa-arrows-h"></i></a>
                                            <a href="{{asset('assets/img/preview/cars/car-600x426x2.jpg')}}" data-gal="prettyPhoto"><img class="img-responsive" src="{{asset('assets/img/preview/cars/car-600x426x2.jpg')}}" alt=""/></a>
                                        </div>
                                    </div>
                                    <div class="row car-thumbnails">
                                        <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [0,300]);"><img src="{{asset('assets/img/preview/cars/car-70x70x1.jpg')}}" alt=""/></a></div>
                                        <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [1,300]);"><img src="{{asset('assets/img/preview/cars/car-70x70x2.jpg')}}" alt=""/></a></div>
                                        <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [2,300]);"><img src="{{asset('assets/img/preview/cars/car-70x70x1.jpg')}}" alt=""/></a></div>
                                        <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [3,300]);"><img src="{{asset('assets/img/preview/cars/car-70x70x2.jpg')}}" alt=""/></a></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="car-details">
                                        <div class="list">
                                            <ul>
                                                <li class="title">
                                                    <h2>{{$auto['Descripcion_Grupo']}}</h2>
                                                    2.0 CC Comfortline 95 HP
                                                </li>
                                                <li>Fuel Diesel / 1600 cm3 Engine</li>
                                                <li>Under 25,000 Km</li>
                                                <li>Transmission Manual</li>
                                                <li>5 Year service included</li>
                                                <li>Manufacturing Year 2014</li>
                                                <li>5 Doors and Panorama View</li>
                                            </ul>
                                        </div>
                                        <div class="price">
                                            <strong>220.0</strong> <span>$/for 8 day(s)</span> <i class="fa fa-info-circle"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="page-divider half transparent"/>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Extras</h3>
                        <form role="form" class="form-extras">

                            <div class="row">
                                <?php
                                    $numExtras = count($extras);

                                    if ($numExtras%2 == 1)
                                    {
                                        $numExtras = round($numExtras/2);
                                    }

                                    $extrasLeft = array_slice($extras, 0, $numExtras-1);
                                    $extrasRight = array_slice($extras, $numExtras);

                                ?>

                                <div class="col-md-6">
                                    <div class="left">
                                        @foreach ($extrasLeft as $extra )
                                            <div class="checkbox checkbox-danger">
                                                <input id="checkboxl1" type="checkbox">
                                                <label for="checkboxl1">{{ $extra['DescripcionExtra'] }}<span class="pull-right">12 $ /for a day</span></label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="right">
                                        @foreach ($extrasRight as $extra )
                                            <div class="checkbox checkbox-danger">
                                                <input id="checkboxl1" type="checkbox">
                                                <label for="checkboxl1">{{ $extra['DescripcionExtra'] }}<span class="pull-right">12 $ /for a day</span></label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        </form>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Información del Cliente</h3>
                        <form action="#" class="form-delivery">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="nombre" id="fd-name" title="Este campo es requerido" data-toggle="tooltip"
                                                class="form-control alt" type="text" placeholder="Nombres*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="apellido" id="fd-lname" title="Este campo es requerido" data-toggle="tooltip"
                                                class="form-control alt" type="text" placeholder="Apellidos*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="email" id="fd-email" title="Este campo es requerido" data-toggle="tooltip"
                                                class="form-control alt" type="text" placeholder="Correo electrónico*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="telefono" class="form-control alt" type="phone" placeholder="Teléfono:">
                                    </div>
                                </div>
                            </div>
                        </form>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Métodos de Pago</h3>
                        <div class="panel-group payments-options" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel radio panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                                            <span class="dot"></span> Efectivo
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                    <div class="panel-body">
                                        <div class="alert alert-success" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapseThree">
                                            <span class="dot"></span> Tarjeta de crédito
                                        </a>
                                <span class="overflowed pull-right">
                                    <img src="{{asset('assets/img/preview/payments/mastercard-2.jpg')}}" alt=""/>
                                    <img src="{{asset('assets/img/preview/payments/visa-2.jpg')}}" alt=""/>
                                    <img src="{{asset('assets/img/preview/payments/american-express-2.jpg')}}" alt=""/>
                                    <img src="{{asset('assets/img/preview/payments/discovery-2.jpg')}}" alt=""/>
                                    <img src="{{asset('assets/img/preview/payments/eheck-2.jpg')}}" alt=""/>
                                </span>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3"></div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading4">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                            <span class="dot"></span> PayPal
                                        </a>
                                        <span class="overflowed pull-right"><img src="{{asset('assets/img/preview/payments/paypal-2.jpg')}}" alt=""/></span>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4"></div>
                            </div>
                        </div>

                        <h3 class="block-title alt"><i class="fa fa-angle-down"></i>Inormación Adicional</h3>
                        <form action="#" class="form-additional">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="fad-message" id="fad-message" title="Addition information is required" data-toggle="tooltip"
                                                class="form-control alt" placeholder="Ingresa aquí comentarios adicionales sobre tu reserva" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="overflowed reservation-now">
                            <div class="checkbox pull-left">
                                <input id="accept" type="checkbox" name="fd-name" title="Please accept" data-toggle="tooltip">
                                <label for="accept">Acepto todos los Términos y Condiciones</label>
                            </div>
                            <a class="btn btn-theme pull-right btn-reservation-now" href="#">Reservar</a>
                            <a class="btn btn-theme pull-right btn-cancel btn-theme-dark" href="#">Cancelar</a>
                        </div>

                    </div>
                    <!-- /CONTENT -->

                    <!-- SIDEBAR -->
                  @include('includes.sidebar')
                    <!-- /SIDEBAR -->

                </div>
            </div>
        </section>
        <!-- /PAGE WITH SIDEBAR -->

        <!-- PAGE -->
        <section class="page-section contact dark">
            <div class="container">

                <!-- Get in touch -->

                <h2 class="section-title">
                    <small>Feel Free to Say Hello!</small>
                    <span>Get in Touch With Us</span>
                </h2>

                <div class="row">
                    <div class="col-md-6">
                        <!-- Contact form -->
                        <form name="contact-form" method="post" action="#" class="contact-form alt" id="contact-form">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="name">Name</label>
                                            <input
                                                    type="text" name="name" id="name" placeholder="Name" value="" size="30"
                                                    data-toggle="tooltip" title="Name is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-user"></i></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="outer required">
                                        <div class="form-group af-inner has-icon">
                                            <label class="sr-only" for="email">Email</label>
                                            <input
                                                    type="text" name="email" id="email" placeholder="Email" value="" size="30"
                                                    data-toggle="tooltip" title="Email is required"
                                                    class="form-control placeholder"/>
                                            <span class="form-control-icon"><i class="fa fa-envelope"></i></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group af-inner has-icon">
                                <label class="sr-only" for="input-message">Message</label>
                                <textarea
                                        name="message" id="input-message" placeholder="Message" rows="4" cols="50"
                                        data-toggle="tooltip" title="Message is required"
                                        class="form-control placeholder"></textarea>
                                <span class="form-control-icon"><i class="fa fa-bars"></i></span>
                            </div>

                            <div class="outer required">
                                <div class="form-group af-inner">
                                    <input type="submit" name="submit" class="form-button form-button-submit btn btn-block btn-theme" id="submit_btn" value="Send message" />
                                </div>
                            </div>

                        </form>
                        <!-- /Contact form -->
                    </div>
                    <div class="col-md-6">

                        <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>

                        <ul class="media-list contact-list">
                            <li class="media">
                                <div class="media-left"><i class="fa fa-home"></i></div>
                                <div class="media-body">Adress: 1600 Pennsylvania Ave NW, Washington, D.C.</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa"></i></div>
                                <div class="media-body">DC 20500, ABD</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-phone"></i></div>
                                <div class="media-body">Support Phone: 01865 339665</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-envelope"></i></div>
                                <div class="media-body">E mails: info@example.com</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-clock-o"></i></div>
                                <div class="media-body">Working Hours: 09:30-21:00 except on Sundays</div>
                            </li>
                            <li class="media">
                                <div class="media-left"><i class="fa fa-map-marker"></i></div>
                                <div class="media-body">View on The Map</div>
                            </li>
                        </ul>

                    </div>
                </div>

                <!-- /Get in touch -->

            </div>
        </section>
        <!-- /PAGE -->

    </div>
    <!-- /CONTENT AREA -->

@endsection
