<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AutoController;

class ReserveController extends Controller{

    private $webservice;
    private $autos;

    public function __construct(){
        $this->autos = new AutoController();
        $this->webservice=New SoapController();
    }


    public function show(Request $request){


        $listaAutos = $this->autos->autos();

        $auto = [];
//        return dd($listaAutos);

        foreach($listaAutos as $autos){
            if($autos['CodGrupoRentCar'] == $request->id){
                $auto = $autos;
            }
        }

        $extras = $request->extras;
        $extras = json_decode($extras, true);
//        return dd($auto);
        //$extras = json_encode($extras);
        $reserva = $request;
    //    return dd($reserva);



        return view('reserve.show', compact('auto', 'extras', 'reserva'));
    }

    public function reservar(Request $request){

        // return dd($request->all());

try {

    $extras=[];

    if(count($request->sw)>0){
        foreach($request->cod_extra as $k=>$v){

            if(in_array($v,$request->sw)){
                $extras[]=[
                    'codigo'=>$v,
                    'nombre'=>$request->descripcion_extra[$k],
                    'cantidad'=>$request->unidades_extra[$k]
                ];
            }

        }
    }

    $datos = [
        "emailCliente" => $request->email,
        "fechaDevolucion" => $request->fechadevolucion,
        "fechaSalida" => $request->fechasalida,
        "grupoVehiculo" => $request->grupo,
        "horaDevolucion" => $request->horadevolucion,
        "horaSalida" => $request->horasalida,
        "nombreCliente" => $request->nombre . ' ' . $request->apellido,
        "numVuelo" => $request->num_vuelo,
        "observacionesCliente" => $request->observaciones,
        "sucursalDevolucion" => $request->sucdevolucion,
        "sucursalSalida" => $request->sucsalida,
        "telefonoMovil" => $request->telefono,
        "totalExtrasConIva" => $request->precio_extras,
        "totalTarifaConIva" => $request->precio_total,
        "extras"=>$extras
    ];

    $res_extras=[];

    $response=$this->webservice->WSReservaRQ($datos);
    $response=json_decode($response, true);
    // return dd($response);
        if(isset ($response['WSReservaRQResponse']['response']['Extras']['Extras1']) && count($response['WSReservaRQResponse']['response']['Extras']['Extras1'])>0 ){

           $res_extras=  $response['WSReservaRQResponse']['response']['Extras']['Extras1'];
            
        }
        $response=$response['WSReservaRQResponse']['response']['Reserva']['Reserva'];
    }
catch (\Exception $e){
    return dd($e);
    $response=[];
}


    return view('reserve.complete',[
        'reserva'=>$response,
        'extras'=>$res_extras
    ]);

    }

}
