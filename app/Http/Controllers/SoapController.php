<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class SoapController extends Controller{

    /**
     * Retorna un array con la respuesta del web service WSDatos
     * @return array $wsdatos
     */

    private $agencia=300;
    private $password=300;
    public function WSDatos(){

        $input_xml = '<e:Envelope xmlns:e="http://schemas.xmlsoap.org/soap/envelope/">
        <e:Body>
          <tns:WSDatos xmlns:tns="http://systinet.com/wsdl/com/unipaas/icm/erent/dickmanns/">
            <tns:Agencia>'.$this->agencia.'</tns:Agencia>
            <tns:Pasword>'.$this->password.'</tns:Pasword>
          </tns:WSDatos>
        </e:Body>
        </e:Envelope>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL, "http://90.68.35.16:6060/dickmanns/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        $response=curl_exec($ch);
        curl_close($ch);

        $fileContents = str_replace(array("\n", "\r", "\t"), '', $response);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $fileContents = trim(str_replace(':', "-", $fileContents));
        $fileContents = trim(str_replace(array(" i-type='d-string'", " i-type='d-decimal'",'wn1-'," i-type='Print_data'"," i-type='ArrayOfExtras'"," i-type='Extras'"," i-type='Sucursales'"," i-type='ArrayOfSucursales'"," i-type='Grupos'"," i-type='ArrayOfGrupos'"," i-type='d-integer'"," i-type='d-boolean'"," i-type='ArrayOfReserva'"," i-type='Reserva'"," i-type='ReservaRS'"," i-nil='true'"), '', $fileContents));
        $fileContents = (str_replace(array("<e-Body>","<WSDatosResponse>","</WSDatosResponse>","</e-Body>","<Status/>","<MensajeError/>","<OtroMensajeError/>","<Sucursales/>","<Extras/>"), '', $fileContents));
        $fileContents = (str_replace(" xmlns-d='http-//www.w3.org/2001/XMLSchema' xmlns-e='http-//schemas.xmlsoap.org/soap/envelope/' xmlns-wn1='http-//systinet.com/wsdl/com/unipaas/icm/erent/dickmanns/' xmlns-wn2='http-//systinet.com/xsd/SchemaTypes/' xmlns-wn0='http-//idoox.com/interface' xmlns-i='http-//www.w3.org/2001/XMLSchema-instance'",'' ,$fileContents));

        //return $fileContents;
        $simpleXml = simplexml_load_string($fileContents);
        $wsdatos = json_encode($simpleXml);
        return $wsdatos;

    }

    public function WSComprobDatos(Request $request){

        $input_xml = '<e:Envelope xmlns:e="http://schemas.xmlsoap.org/soap/envelope/">
              <e:Body>
              <tns:WSComprobDatos xmlns:tns="http://systinet.com/wsdl/com/unipaas/icm/erent/dickmanns/">
                  <tns:FechaSalida_Alfa>'.$request->fechaSalida.'</tns:FechaSalida_Alfa>
                  <tns:HoraSalida_Alfa>'.$request->horaSalida.'</tns:HoraSalida_Alfa>
                  <tns:FechaDevolucion_Alfa>'.$request->fechaDevolucion.'</tns:FechaDevolucion_Alfa>
                  <tns:HoraDevolucion_Alfa>'.$request->horaDevolucion.'</tns:HoraDevolucion_Alfa>
                  <tns:SucursalSalida>'.$request->sucursalSalida.'</tns:SucursalSalida>
                  <tns:SucursalDevolucion>'.$request->sucursalDevolucion.'</tns:SucursalDevolucion>
                  <tns:AgenciaPar>'.$this->agencia.'</tns:AgenciaPar>
                  <tns:Pasword>'.$this->password.'</tns:Pasword>
              </tns:WSComprobDatos>
              </e:Body>
        </e:Envelope>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL, "http://90.68.35.16:6060/dickmanns/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        $response=curl_exec($ch);
        curl_close($ch);

        $fileContents = str_replace(array("\n", "\r", "\t"), '', $response);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $fileContents = trim(str_replace(':', "-", $fileContents));
        $fileContents = trim(str_replace(array(" i-type='d-string'", " i-type='d-decimal'",'wn1-'," i-type='Print_data'"," i-type='ArrayOfExtras'"," i-type='Extras'"," i-type='Sucursales'"," i-type='ArrayOfSucursales'"," i-type='Grupos'"," i-type='ArrayOfGrupos'"," i-type='d-integer'"," i-type='d-boolean'"," i-type='ArrayOfReserva'"," i-type='Reserva'"," i-type='ReservaRS'"," i-nil='true'"), '', $fileContents));
        $fileContents = (str_replace(array("<e-Body>","<WSDatosResponse>","</WSDatosResponse>","</e-Body>","<Status/>","<MensajeError/>","<OtroMensajeError/>","<Sucursales/>","<Extras/>"), '', $fileContents));
        $fileContents = (str_replace(" xmlns-d='http-//www.w3.org/2001/XMLSchema' xmlns-e='http-//schemas.xmlsoap.org/soap/envelope/' xmlns-wn1='http-//systinet.com/wsdl/com/unipaas/icm/erent/dickmanns/' xmlns-wn2='http-//systinet.com/xsd/SchemaTypes/' xmlns-wn0='http-//idoox.com/interface' xmlns-i='http-//www.w3.org/2001/XMLSchema-instance'",'' ,$fileContents));

  		$simpleXml = simplexml_load_string($fileContents);
//  		return dd($simpleXml);
  		$json = json_encode($simpleXml);
        return $json;
        
    }


      public function WSReservaRQ($datos){

        $extras='';

          if(count($datos['extras'])>0){
            $extras.=' <tns:Extras>';
                foreach($datos['extras'] as $k=>$v){
                   $extras.=' <tns:Extras2>
                       <tns:CodExtraRQ>'.$v['codigo'].'</tns:CodExtraRQ>
                       <tns:NombreRQ>'.$v['nombre'].'</tns:NombreRQ>
                        <tns:UnidadesRQ>'.$v['cantidad'].'</tns:UnidadesRQ>
                       </tns:Extras2>';
                }
                $extras.=' </tns:Extras>';
              }

        $input_xml = '<e:Envelope xmlns:e="http://schemas.xmlsoap.org/soap/envelope/">
          <e:Body>
            <tns:WSReservaRQ xmlns:tns="http://systinet.com/wsdl/com/unipaas/icm/erent/dickmanns/">
              <tns:ReservaRQ>';

                if(count($datos['extras'])>0){
                  $input_xml.=$extras;
                }
                else{
              $input_xml.='<tns:Extras/>';
                }
                          
              
                 $input_xml.='<tns:Reserva>
                  <tns:Reserva1>
                    <tns:AgenciaRQ>'.$this->agencia.'</tns:AgenciaRQ>
                    <tns:EMailClienteRQ>'.$datos["emailCliente"].'</tns:EMailClienteRQ>
                    <tns:FechaDevolucionRQ>'.$datos["fechaDevolucion"].'</tns:FechaDevolucionRQ>
                    <tns:FechaSalidaRQ>'.$datos["fechaSalida"].'</tns:FechaSalidaRQ>
                    <tns:GrupoVehiculoRQ>'.$datos["grupoVehiculo"].'</tns:GrupoVehiculoRQ>
                    <tns:HoraDevolucionRQ>'.$datos["horaDevolucion"].'</tns:HoraDevolucionRQ>
                    <tns:HoraSalidaRQ>'.$datos["horaSalida"].'</tns:HoraSalidaRQ>
                    <tns:NombreClienteRQ>'.$datos["nombreCliente"].'</tns:NombreClienteRQ>
                    <tns:NumVueloRQ>'.$datos["numVuelo"].'</tns:NumVueloRQ>
                    <tns:ObservacionesClienteRQ>'.$datos["observacionesCliente"].'</tns:ObservacionesClienteRQ>
                    <tns:PasswordRQ>'.$this->password.'</tns:PasswordRQ>
                    <tns:SucDevolucionRQ>'.$datos["sucursalDevolucion"].'</tns:SucDevolucionRQ>
                    <tns:SucSalidaRQ>'.$datos["sucursalSalida"].'</tns:SucSalidaRQ>
                    <tns:TelefonoMovilRQ>'.$datos["telefonoMovil"].'</tns:TelefonoMovilRQ>
                    <tns:TotalExtrasConIvaRQ>'.$datos["totalExtrasConIva"].'</tns:TotalExtrasConIvaRQ>
                    <tns:TotalTarifaConIvaRQ>'.$datos["totalTarifaConIva"].'</tns:TotalTarifaConIvaRQ>
                  </tns:Reserva1>
                </tns:Reserva>
              </tns:ReservaRQ>
            </tns:WSReservaRQ>
          </e:Body>
        </e:Envelope>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL, "http://90.68.35.16:6060/dickmanns/");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        $response=curl_exec($ch);
        curl_close($ch);

        $fileContents = str_replace(array("\n", "\r", "\t"), '', $response);
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $fileContents = trim(str_replace(':', "-", $fileContents));

        $fileContents = trim(str_replace(array(" i-type='d-string'", " i-type='d-decimal'",'wn1-'," i-type='Print_data'"," i-type='ArrayOfExtras'"," i-type='Extras'"," i-type='Sucursales'"," i-type='ArrayOfSucursales'"," i-type='Grupos'"," i-type='ArrayOfGrupos'"," i-type='d-integer'"," i-type='d-boolean'"," i-type='ArrayOfReserva'"," i-type='Reserva'"," i-type='ReservaRS'"," i-nil='true'"), '', $fileContents));

        $fileContents = (str_replace(array("<e-Body>","<WSDatosResponse>","</WSDatosResponse>","</e-Body>","<Status/>","<MensajeError/>","<OtroMensajeError/>","<Sucursales/>","<Extras/>"), '', $fileContents));


        $fileContents = (str_replace(" xmlns-d='http-//www.w3.org/2001/XMLSchema' xmlns-e='http-//schemas.xmlsoap.org/soap/envelope/' xmlns-wn1='http-//systinet.com/wsdl/com/unipaas/icm/erent/dickmanns/' xmlns-wn2='http-//systinet.com/xsd/SchemaTypes/' xmlns-wn0='http-//idoox.com/interface' xmlns-i='http-//www.w3.org/2001/XMLSchema-instance'",'' ,$fileContents));

        //return $fileContents;
        $simpleXml = simplexml_load_string($fileContents);
        $json = json_encode($simpleXml);
        return $json;
      }

}
