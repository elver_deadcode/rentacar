<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\AutoController;

class StaticController extends Controller{

    private $autos;

    public function __construct(){
        $this->autos = new AutoController();
    }

    public function home(){
        $datos=[];
        $sucursales = $this->autos->sucursales();
        if(count($sucursales)>0)
            $datos=$sucursales;
        return view('home', compact('datos'));
    }

}
