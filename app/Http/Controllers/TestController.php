<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SoapController;

class TestController extends Controller{

    private $webservice;

    public function __construct(){
        $this->webservice = new SoapController;
    }

    public function wsdatos(){
        $wsdatos = $this->webservice->wsdatos();
        return $wsdatos;
    }

}
