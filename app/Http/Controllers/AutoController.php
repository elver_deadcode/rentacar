<?php

namespace App\Http\Controllers;
use \Illuminate\Http\Request;
use App\Http\Controllers\SoapController;
use View;

class AutoController extends Controller
{

    protected $webservice;

    public function __construct(){
        $this->webservice = new SoapController();
    }

    /**
	 * Muestra una lista de los autos.
	 *
	 * @return array $autos
	 */
    public function autos(){
		$autos = $this->webservice->wsdatos();
        $autos = json_decode($autos, true);
        $autos = $autos["response"]['Grupos']['Grupos'];
        //$autos = json_encode($autos);
        return $autos;

    }

    /**
	 * Retorna una lista de extras.
	 *
	 * @return array $extras
	 */
    public function extras(){

		$extras = $this->webservice->wsdatos();
        $extras = json_decode($extras, true);
        $extras = $extras["response"]['Extras']['Extras'];

        return $extras;

    }

    /**
     * Retorna la lista de sucursales
     * @return array $sucursales
     */
    public function sucursales(){

        $sucursales=[];

        $sucursales = $this->webservice->wsdatos();
        $sucursales = json_decode($sucursales, true);
        // if(isset($sucursales['response'])){
        $sucursales = $sucursales['response']['Sucursales']['Sucursales'];
        // }

        return $sucursales;

    }


    /**
	 * Muestra lista de autos despues una búsqueda.
	 *
	 * @return array $autos
     * @return view car.search
	 */
	public function search(Request $request){
//	    return dd($request->all());

		$respuesta = $this->webservice->WSComprobDatos($request);

		$respuesta = json_decode($respuesta, true);
//		return dd($respuesta);
        $autos = $respuesta["WSComprobDatosResponse"]["response"]["Grupos"]["Grupos"];
        $reserva = $request;
       $extras=[];
       if(isset($respuesta['WSComprobDatosResponse']['response']['Extras']) ) {
           $extras = $respuesta['WSComprobDatosResponse']['response']['Extras']['Extras'];
      $extras = json_encode($extras);
       }
//       return dd($autos);
        return view('car.search', [
            'autos'=>$autos,
            'extras'=>$extras,
            'reserva'=>$request
        ]);

	}


	public function show($id){

        $listaAutos = $this->index();
        $listaAutos = json_decode($listaAutos);

        $auto = [];

        foreach($listaAutos as $autos){

            if($autos->CodGrupoRentCar == $id){
                $auto = $autos;
            }

        }

        $extras = $this->extras();

        return view('car.show', compact('auto', 'extras'));

    }

}
