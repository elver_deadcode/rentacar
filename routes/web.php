<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Paginas estaticas
Route::get('/', 'StaticController@home')->name('home');

//Rutas Post para la reserva de autos
Route::post('buscar-auto', 'AutoController@search')->name('buscar-auto');
Route::post('reservar-auto', 'ReserveController@show')->name('reservar-auto');
Route::post('reservar-request', 'ReserveController@reservar');
Route::get('reservar-status', 'ReserveController@status');


//Rutas para development
Route::get('dev/wsdatos', 'TestController@wsdatos');

